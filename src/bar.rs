use super::*;

use std::path::Path;

fn default_max_lag() -> Duration { Duration::from_millis(50) }

#[derive(serde::Deserialize)]
pub struct Config
{
    #[serde(default="default_max_lag")]
    pub max_lag: Duration,
    pub blocks:  Vec<block::Config>
}

pub struct Bar
{
    max_lag:       Duration,
    blocks:        Vec<block::Block>,
    content_types: HashMap<String, Box<dyn content::ContentTypeBoxed>>
}

fn parse_instance(event: &swaybar_types::Click) -> Option<usize>
{
    event.instance.as_ref()?.parse::<usize>().ok()
}

impl Bar
{
    pub fn with_content_type<T>(mut self, name: &str, typ: T)
        -> Self
        where T: content::ContentType + Sized + 'static
    {
        let boxed = Box::new(typ);
        self.content_types.insert(String::from(name), boxed);

        self
    }

    pub async fn with_config(mut self, config: &Config)
        -> Result<Self>
    {
        self.max_lag = std::cmp::min(config.max_lag, self.max_lag);

        for (i, conf) in config.blocks.iter().enumerate()
        {
            let new = block::Block::new(&conf, &self.content_types, i)
                .await
                .map_err(with_note!("Error adding block {}", i))?;

            self.blocks.push(new);
        }

        Ok(self)
    }

    pub async fn with_config_str(self, config: &str) -> Result<Self>
    {
        let config: Config = toml::from_str(&config)
            .map_err(with_note!("Error parsing config"))?;

        self.with_config(&config).await
    }

    pub async fn with_config_path(self, path: impl AsRef<Path>)
        -> Result<Self>
    {
        let conf_str = fs::read_to_string(path.as_ref()).await
            .map_err(with_note!("Error reading config {:?}", path.as_ref()))?;

        self.with_config_str(&conf_str).await
    }

    pub async fn await_status(&mut self)
    {
        let mut futures: FuturesUnordered<_> = self.blocks.iter_mut()
            .map(|blk| blk.await_status())
            .collect();

        futures.next().await;

        sleep(self.max_lag).await;
    }

    pub fn serialise(&mut self) -> Vec<swaybar_types::Block>
    {
        self.blocks.iter_mut()
            .flat_map(|blk| blk.serialise())
            .collect()
    }

    pub async fn handle_event(&mut self, event: swaybar_types::Click)
    {
        if let Some(inst) = parse_instance(&event)
        {
            if let Some(blk) = self.blocks.get(inst)
            {
                blk.click().await;
            }
        }
    }
}

impl Default for Bar
{
    fn default() -> Self
    {
        Self
        {
            max_lag:       Duration::from_secs(10),
            blocks:        Vec::new(),
            content_types: HashMap::new()
        }
    }
}
