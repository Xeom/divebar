use super::*;

use swaybar_types::*;

#[derive(Default)]
pub struct JsonBuf
{
    /// A buffer of bytes currently being examined.
    ///
    /// Both `AsyncReadExt.read_buf()`, and `serde_json::Deserializer::from_*`
    /// are easier to work with if our JSON is in contiguous slices, so it is
    /// easiest if we use a `Vec` as our buffer type, and keep a count of the
    /// number of bytes consumed from the front.
    ///
    /// This approach should result in no-allocations or moves being made, as
    /// long as we're handling events faster than they arrive.
    ///
    buf: Vec<u8>,
    /// The number of bytes consumed from the start of the buffer.
    consumed: usize
}

impl JsonBuf
{
    /// Poll the input stream for new bytes and append them to the JSON buffer.
    ///
    /// # Parameters
    ///
    ///  * `input` - Where to read the new bytes from.
    ///
    /// # Cancel Safety
    ///
    /// This method is cancel safe.
    ///
    async fn read<I>(&mut self, input: &mut I)
        -> io::Result<()>
        where I: AsyncRead + Unpin
    {
        // If over half our buffer is consumed, we move the remaining contents
        // to the start of the vector.
        //
        // In the most common case, we should have consumed an entire JSON
        // object, and so the buffer will be empty, so no moving will be
        // necessary.
        //
        // By using `.drain()`, we also ensure that the buffer is not
        // reallocated.
        if self.consumed >= self.buf.len() / 2
        {
            // We remove our consumed elements from the vector.
            self.buf.drain(..self.consumed);
            // We reset the counter of consumed items.
            self.consumed = 0;
        }

        input.read_buf(&mut self.buf).await?;

        Ok(())
    }

    /// Get the current unconsumed-JSON buffer.
    fn get(&self) -> &[u8]
    {
        &self.buf[self.consumed..]
    }

    /// Consume bytes from the start of the JSON buffer.
    ///
    /// # Parameters
    ///
    ///  * `bytes` - The number of bytes to consume.
    ///
    fn consume(&mut self, bytes: usize)
    {
        self.consumed = std::cmp::min(self.buf.len(), self.consumed + bytes);
    }

    /// Consume all remaining bytes from the JSON buffer.
    fn consume_all(&mut self)
    {
        self.consumed = self.buf.len();
    }
}

/// A context for a protocol.
pub struct Protocol<I, O>
{
    /// Input JSON stream.
    input: I,
    /// Output JSON stream.
    output: O,
    /// Buffer for JSON.
    json:   JsonBuf
}

pub fn default_header() -> Header
{
    Header
    {
        version:      Version::One,
        stop_signal:  None,
        cont_signal:  None,
        click_events: Some(true)
    }
}

impl<I, O> Protocol<I, O>
{
    pub fn new(input: I, output: O) -> Self
    {
        Self { input, output, json: JsonBuf::default() }
    }
}

impl<I, O> Protocol<I, O>
    where I: AsyncRead + Unpin
{
    /// Consume until the next character is an open bracket.
    ///
    /// # Cancel Safety
    ///
    /// This method is cancel safe.
    ///
    async fn consume_to_next_bracket(&mut self) -> io::Result<()>
    {
        // Consume characters until we reach the next separator.
        'find_ch: loop
        {
            let json_buf = self.json.get();

            // Find the position of the first '{' character.
            let index = json_buf.iter().position(|&ch| ch == b'{');

            match index
            {
                // We found the position, so consume up until it and break...
                Some(ind) =>
                {
                    self.json.consume(ind);
                    break 'find_ch;
                }
                // No matching charatcer was found, so consume the whole slice
                // and poll for more...
                None =>
                {
                    self.json.consume_all();
                    self.json.read(&mut self.input).await?;
                }
            }
        }

        Ok(())
    }

    /// Consume and return a `Click` event structure.
    ///
    /// # Cancel Safety
    ///
    /// This method is cancel safe.
    /// If cancelled, no data is discarded, and a consuming the click event may
    /// be resumed.
    ///
    async fn consume_next_event(&mut self) -> io::Result<Click>
    {
        loop
        {
            let json_buf  = self.json.get();
            let mut deser = serde_json::Deserializer::from_slice(json_buf)
                .into_iter();

            match deser.next()
            {
                // Successfully found an event
                Some(Ok(clk)) =>
                {
                    self.json.consume(deser.byte_offset());
                    return Ok(clk);
                }
                // The JSON object ended early!
                None =>
                {
                    self.json.read(&mut self.input).await?;
                }
                Some(Err(e)) if e.is_eof() =>
                {
                    self.json.read(&mut self.input).await?;
                }
                // The JSON object was invalid!
                Some(Err(e)) =>
                {
                    // Discard the whole buffer!
                    self.json.consume_all();
                    // Return an error
                    return Err(io::Error::new(
                        io::ErrorKind::Other, format!("{}", e)
                    ));
                }
            }
        }
    }

    /// Parse the next event.
    ///
    /// # Cancel Safety.
    ///
    /// This method is cancel safe.
    /// If cancelled, no data is discarded, and a consuming the click event may
    /// be resumed.
    ///
    pub async fn next_event(&mut self) -> io::Result<Click>
    {
        // Ensure that the next character is an open bracket '{' ...
        self.consume_to_next_bracket().await?;

        // Consume the next event...
        self.consume_next_event().await
    }
}

impl<I, O> Protocol<I, O>
    where O: AsyncWrite + Unpin
{
    /// Send the current `Bar` status.
    pub async fn send_bar(&mut self, bar: &[Block]) -> io::Result<()>
    {
        let json = serde_json::to_string(bar)
            .map_err(|e| io::Error::new(
                io::ErrorKind::Other, format!("{}", e)
            ))?;
        self.output.write_all(json.as_bytes()).await?;
        self.output.write_all(b",\n").await?;

        Ok(())
    }

    /// Send the initial header.
    pub async fn send_header(&mut self) -> io::Result<()>
    {
        let json = serde_json::to_string(&default_header())
            .map_err(|e| io::Error::new(
                io::ErrorKind::Other, format!("{}", e)
            ))?;
        self.output.write_all(json.as_bytes()).await?;
        self.output.write_all(b"\n[\n").await?;

        Ok(())
    }
}

impl<I, O> Protocol<I, O>
    where I: AsyncRead + Unpin, O: AsyncWrite + Unpin
{
    pub async fn run_with_bar(mut self, mut bar: bar::Bar) -> io::Result<()>
    {
        self.send_header().await?;
        self.send_bar(&bar.serialise()).await?;

        loop
        {
            tokio::select!(
                _ = bar.await_status() =>
                {
                    self.send_bar(&bar.serialise()).await?;
                }
                event = self.next_event() =>
                {
                    bar.handle_event(event?).await;
                }
            );
        }
    }
}
