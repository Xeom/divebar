use super::*;

pub struct Status
{
    pub full:   Option<String>,
    pub short:  Option<String>,
    pub urgent: bool
}

impl Default for Status
{
    fn default() -> Self
    {
        Self { full: None, short: None, urgent: false }
    }
}

impl From<io::Error> for Status
{
    fn from(e: io::Error) -> Self
    {
        Status
        {
            full:   Some(format!("{}: {:?}", e, e)),
            short:  Some(format!("{}", e)),
            urgent: true
        }
    }
}

impl Status
{
    fn full_ref(&self) -> Option<&str> { self.full.as_deref() }

    fn short_ref(&self) -> Option<&str> { self.short.as_deref() }

    fn get_full(&self, conf: &Config) -> Option<String>
    {
        let content = self.full_ref()?;

        let prefix = conf.prefix.as_deref()
            .unwrap_or("");

        Some(format!("{pre}{cont}", pre=prefix, cont=content))
    }

    fn get_short(&self, conf: &Config) -> Option<String>
    {
        // If the short status would evaluate to the same as the full
        // status, we can just return None.
        if self.short.is_none() && conf.prefix_short.is_none()
        {
            return None
        }

        let content = self.short_ref()
            .or(self.full_ref())?;

        let prefix = conf.prefix_short.as_deref()
            .or(conf.prefix.as_deref())
            .unwrap_or("");

        Some(format!("{pre}{cont}", pre=prefix, cont=content))
    }
}

fn default_alignment() -> swaybar_types::Align { swaybar_types::Align::Right }
fn default_separate()  -> bool                 { true }
fn default_min_sleep() -> Duration             { Duration::from_millis(500) }
fn default_max_sleep() -> Duration             { Duration::from_secs(300)   }

#[derive(serde::Deserialize, Clone)]
pub struct Config
{
    /// The type of content to display in this block.
    ///
    /// This must have been previously registered to the `Bar` using the
    /// `.with_content_type()` method.
    ///
    pub content_type: String,

    /// The alignment to use for the contents of this block.
    #[serde(default="default_alignment")]
    pub alignment: swaybar_types::Align,

    /// Whether to separate this block from the next.
    #[serde(default="default_separate")]
    pub separate: bool,

    /// The minimum amount of time to wait before updating this block.
    ///
    /// Some content-types are able to intelligently wait until they are ready
    /// to be polled, (e.g. `"backlight"`, `"time"`), but some will simply
    /// constantly poll unless given a minimum amount of time to sleep before
    /// updating. (e.g. `"wifi"`, `"battery"`)
    ///
    #[serde(default="default_min_sleep")]
    pub min_wait: Duration,

    /// The maximum amount of time to wait before updating this block.
    ///
    /// This is useful for blocks that try and wait too long before updating
    /// themselves. For example a `"backlight"` block where the underlying
    /// backlight value changes without accesses to the VFS.
    ///
    #[serde(default="default_max_sleep")]
    pub max_wait: Duration,

    /// A label to prefix the block with.
    ///
    /// This label will be placed before the block text.
    ///
    #[serde(rename="prefix_label")]
    pub prefix: Option<String>,

    /// A label to suffix the shortened block with.
    ///
    /// This label will be placed before the block when it is shortened. If
    /// `prefix_label` is present, it is used by default for both full and
    /// shortened blocks, but is overriden by this option.
    ///
    /// Blocks are shortened by swaybar when the bar becomes full.
    ///
    #[serde(rename="prefix_short_label")]
    pub prefix_short: Option<String>,

    /// Inner content-type specific configuration.
    ///
    /// This is deserialized into a content-type specific config structure,
    /// and passed to the `ContentType` trait.
    ///
    #[serde(flatten)]
    pub inner: toml::Value
}

pub struct Block
{
    config:    Config,
    name:      String,
    instance:  usize,
    click_tx:  mpsc::Sender<()>,
    status_rx: watch::Receiver<Status>
}

async fn new_content(
    config:        &Config,
    content_types: &HashMap<String, Box<dyn content::ContentTypeBoxed>>
)
    -> Result<Box<dyn content::Content>>
{
    if let Some(found) = content_types.get(&config.content_type)
    {
        found.new_boxed_content(config).await
    }
    else
    {
        Err(err!("Unknown content type {:?}", config.content_type))
    }
}

enum Event { Click, Status(io::Result<Status>) }

async fn block_task(
    mut content:  Box<dyn content::Content>,
    mut click_rx: mpsc::Receiver<()>,
    status_tx:    watch::Sender<Status>,
    config:       Config
)
{
    let cancellable = content.status_cancellable();

    loop
    {
        let event = if cancellable
            {
                tokio::select!(
                    status  = content.status() => { Event::Status(status) }
                    Some(_) = click_rx.recv()  => { Event::Click          }
                )
            }
            else
            {
                if let Ok(_) = click_rx.try_recv()
                {
                    Event::Click
                }
                else
                {
                    Event::Status(content.status().await)
                }
            };

        match event
        {
            Event::Status(status) =>
            {
                status_tx.send(status.unwrap_or_else(From::from)).ok();
                tokio::join!(
                    async
                    {
                        tokio::select!(
                            res = content.wait()         => { res.ok(); },
                            _   = sleep(config.max_wait) => {}
                        )
                    },
                    sleep(config.min_wait)
                );
            }
            Event::Click =>
            {
                content.click().await.ok();
            }
        }
    }
}

fn spawn_block_task(content: Box<dyn content::Content>, config: &Config)
    -> (mpsc::Sender<()>, watch::Receiver<Status>, task::JoinHandle<()>)
{
    let (click_tx,  click_rx)  = mpsc::channel(8);
    let (status_tx, status_rx) = watch::channel(Status::default());

    let join = task::spawn(block_task(
        content, click_rx, status_tx, config.clone()
    ));

    (click_tx, status_rx, join)
}

impl Block
{
    pub fn serialise(&mut self) -> Option<swaybar_types::Block>
    {
        // Using borrow_and_update marks the value as seen.
        let status = self.status_rx.borrow_and_update();

        Some(swaybar_types::Block
        {
            full_text:             status.get_full(&self.config)?,
            short_text:            status.get_short(&self.config),
            color:                 None,
            background:            None,
            border:                None,
            border_bottom:         None,
            border_top:            None,
            border_right:          None,
            border_left:           None,
            min_width:             None,
            align:                 Some(self.config.alignment),
            name:                  Some(self.name.clone()),
            instance:              Some(self.instance.to_string()),
            urgent:                Some(status.urgent),
            separator:             Some(self.config.separate),
            separator_block_width: Some(25),
            markup:                None
        })
    }

    pub async fn await_status(&mut self)
    {
        self.status_rx.changed().await.ok();
    }

    pub async fn click(&self)
    {
        self.click_tx.send(()).await.ok();
    }

    pub async fn new(
        config:        &Config,
        content_types: &HashMap<String, Box<dyn content::ContentTypeBoxed>>,
        instance:      usize
    )
        -> Result<Self>
    {
        let name    = String::from("block");
        let content = new_content(config, content_types).await?;

        let (click_tx, status_rx, _join) = spawn_block_task(content, config);

        Ok(Self { config: config.clone(), instance, name, click_tx, status_rx })
    }
}

