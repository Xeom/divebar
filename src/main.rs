use tokio::{
    sync::{watch, mpsc},
    time::{Duration, sleep},
    task,
    fs,
    io::{self, AsyncWrite, AsyncWriteExt, AsyncRead, AsyncReadExt},
};
use futures::stream::{FuturesUnordered, StreamExt};
use std::collections::HashMap;
use async_trait::async_trait;

mod protocol;
mod content;
mod block;
mod bar;
mod err;
use err::{err, note, with_note, Error, Result};

#[tokio::main]
async fn main() -> Result<(), err::DisplayError>
{
    let conf = std::env::args().nth(1)
        .ok_or(err!("Expected a config path argument"))?;

    let proto = protocol::Protocol::new(io::stdin(), io::stdout());

    let bar = bar::Bar::default()
        .with_content_type("time",      content::Time)
        .with_content_type("battery",   content::Bat)
        .with_content_type("backlight", content::Backlight)
        .with_content_type("wifi",      content::Wifi)
        .with_config_path(conf).await?;

    proto.run_with_bar(bar).await.map_err(<Error as From<_>>::from)?;

    Ok(())
}
