use super::*;

use std::{fmt, error};

pub type Result<T=(), E=Error> = std::result::Result<T, E>;

type Boxed = Box<dyn error::Error + Sync + Send + 'static>;

#[derive(Clone, Copy, Debug)]
pub enum ErrorKind
{
    Io,
    Battery,
    Unknown,
    Toml
}

pub struct Note
{
    pub line: u32,
    pub file: &'static str,
    pub note: String
}

pub struct Error
{
    kind:  ErrorKind,
    notes: Vec<Note>,
    cause: Option<Boxed>
}

impl fmt::Display for Note
{
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        write!(fmt, "{}:{} {}", self.file, self.line, self.note)
    }
}

impl fmt::Display for Error
{
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        write!(fmt, "Error - {:?}\n", self.kind)?;
        for note in self.notes.iter().rev() { write!(fmt, "    {}\n", note)?; }

        Ok(())
    }
}

impl fmt::Debug for Error
{
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        <Self as fmt::Display>::fmt(self, fmt)
    }
}

impl error::Error for Error
{
    fn source(&self) -> Option<&(dyn error::Error + 'static)>
    {
        self.cause.as_deref().map(|x| x as _)
    }
}

impl Default for Error
{
    fn default() -> Error
    {
        Error { kind: ErrorKind::Unknown, notes: Vec::new(), cause: None }
    }
}

impl From<io::Error> for Error
{
    fn from(err: io::Error) -> Error
    {
        Error::default()
            .with_cause(err)
            .with_kind(ErrorKind::Io)
    }
}

impl From<battery::Error> for Error
{
    fn from(err: battery::Error) -> Error
    {
        Error::default()
            .with_cause(err)
            .with_kind(ErrorKind::Battery)
    }
}

impl From<toml::de::Error> for Error
{
    fn from(err: toml::de::Error) -> Error
    {
        Error::default()
            .with_cause(err)
            .with_kind(ErrorKind::Toml)
    }
}

impl Error
{
    pub fn with_cause<E>(mut self, cause: E) -> Self
        where Boxed: From<E>
    {
        self.cause = Some(Boxed::from(cause));
        self
    }

    pub fn with_kind(mut self, kind: ErrorKind) -> Self
    {
        self.kind = kind;
        self
    }

    pub fn with_note(mut self, note: Note) -> Self
    {
        self.notes.push(note);
        self
    }
}

macro_rules! err
{
    ($($fmt:tt)*) =>
    {
        note!(Error::default(), $($fmt)*)
    }
}

macro_rules! note
{
    ($err:expr, $($fmt:tt)*) =>
    {
        Error::from($err).with_note(
            crate::err::Note {
                note: format!($($fmt)*),
                file: file!(),
                line: line!()
            }
        )
    }
}

macro_rules! with_note
{
    ($($fmt:tt)*) =>
    {
        |err| { note!(err, $($fmt)*) }
    }
}

pub(crate) use err;
pub(crate) use note;
pub(crate) use with_note;

pub struct DisplayError(Error);

impl fmt::Display for DisplayError
{
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        use std::error::Error;
        write!(fmt, "{}\n", self.0)?;

        let first = self.0.source();

        if first.is_some() { write!(fmt, "Caused By:\n")?; }
        for (i, cause) in std::iter::successors(first, |&e| e.source())
            .enumerate()
        {
            write!(fmt, "    [{}] {}\n", i, cause)?;
        }

        Ok(())
    }
}

impl fmt::Debug for DisplayError
{
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        <Self as fmt::Display>::fmt(self, fmt)
    }
}


impl<E> From<E> for DisplayError where Error: From<E>
{
    fn from(e: E) -> DisplayError { DisplayError(From::from(e)) }
}
