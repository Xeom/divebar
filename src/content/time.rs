use super::*;

pub struct Time;
pub struct TimeInst;

const SHORT_FORMAT: &'static str = "%e %b %R";
const LONG_FORMAT:  &'static str = "%a %e %b %y %T";

#[async_trait]
impl ContentType for Time
{
    type Inst   = TimeInst;
    type Config = EmptyConfig;

    async fn new_content(&self, _: EmptyConfig) -> Result<TimeInst>
    {
        Ok(TimeInst)
    }
}

async fn sleep_until_next_second()
{
    let now = chrono::Local::now();
    let millis = now.timestamp_subsec_millis() as u64;
    let duration = Duration::from_millis(1000u64 - millis);

    sleep(duration).await;
}

#[async_trait]
impl Content for TimeInst
{
    /// Wait until the time changes.
    async fn wait(&mut self) -> io::Result<()>
    {
        // Wait until the second changes!
        sleep_until_next_second().await;

        Ok(())
    }

    /// Get the current time.
    async fn status(&mut self) -> io::Result<block::Status>
    {
        let now = chrono::Local::now();

        Ok(block::Status {
            full:   Some(format!("{}", now.format(LONG_FORMAT))),
            short:  Some(format!("{}", now.format(SHORT_FORMAT))),
            urgent: false
        })
    }
}
