use super::*;

use tokio::process::Command;

struct WifiInfo
{
    ssid:   String,
    signal: u64,
    active: bool
}

impl From<&str> for WifiInfo
{
    fn from(s: &str) -> WifiInfo
    {
        let mut split = s.split(":");

        let ssid = split.next()
            .unwrap_or("Unknown-SSID")
            .to_owned();
        let signal = split.next()
            .and_then(|s| s.parse::<u64>().ok())
            .unwrap_or(0);
        let active = split.next()
            .map(|s| s == "yes")
            .unwrap_or(false);

        Self { ssid, signal, active }
    }
}

async fn get_wifi_info() -> io::Result<Vec<WifiInfo>>
{
    let out = Command::new("nmcli")
        .arg("-t")
        .arg("-f").arg("ssid,signal,active")
        .arg("device")
        .arg("wifi")
        .output()
        .await?;

    let rtn = std::str::from_utf8(&out.stdout)
        .map_err(|e| io::Error::new(
            io::ErrorKind::Other,
            format!("Could not convert to UTF-8: {}", e)
        ))?
        .lines()
        .map(str::trim)
        .filter(|s| s.len() > 0)
        .map(<WifiInfo as From<&str>>::from)
        .collect();

    Ok(rtn)
}

fn build_status(
    info: &[WifiInfo],
    f: impl Fn(&WifiInfo) -> String
)
    -> Option<String>
{
    let strings = info.iter()
        .filter(|info| info.active)
        .map(f)
        .collect::<Vec<String>>();

    if strings.len() > 0 { Some(strings.join(", ")) } else { None }
}

pub struct Wifi;
pub struct WifiInst;

#[async_trait]
impl ContentType for Wifi
{
    type Inst   = WifiInst;
    type Config = EmptyConfig;

    async fn new_content(&self, _: EmptyConfig) -> Result<WifiInst>
    {
        Ok(WifiInst)
    }
}

#[async_trait]
impl Content for WifiInst
{
    async fn status(&mut self) -> io::Result<block::Status>
    {
        let info = get_wifi_info().await?;

        let fmt_full  = |i: &WifiInfo| format!("{} {:3.0}%", i.ssid, i.signal);
        let fmt_short = |i: &WifiInfo| format!("{}", i.ssid);

        Ok(block::Status {
            full:   build_status(&info, fmt_full),
            short:  build_status(&info, fmt_short),
            urgent: false
        })
    }
}

