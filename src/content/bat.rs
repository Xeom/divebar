use super::*;

#[derive(serde::Deserialize)]
pub struct Icons
{
    unknown:     String,
    charging:    String,
    discharging: Vec<String>,
    empty:       String,
    full:        String
}

impl Icons
{
    fn get(&self, state: battery::State, perc: f32) -> &str
    {
        use battery::State::*;
        match state
        {
            Full               => &self.full,
            Discharging        => self.get_discharging(perc),
            Empty              => &self.empty,
            Charging           => &self.charging,
            // My laptop gives State::Unknown when full!
            _ if perc == 100.0 => &self.full,
            Unknown | _        => &self.unknown,
        }
    }

    fn get_discharging(&self, perc: f32) -> &str
    {
        let approx = (perc / 101.0) * self.discharging.len() as f32;
        let ind = approx.floor() as usize;
        self.discharging.get(ind).unwrap_or(&self.unknown)
    }
}

#[derive(serde::Deserialize)]
pub struct BatConfig
{
    icons: Option<Icons>
}

pub struct Bat;
pub struct BatInst
{
    mngr: battery::Manager,
    info: battery::Battery,
    conf: BatConfig
}

// The `batter::Manager` struct is neither `Sync` nor `Send`.
//
// As far as I can tell, everything is okay with this, though we can protect
// this with a `Mutex` if needed.
//
// The right solution would be to complain about the `battery` crate.
unsafe impl Sync for BatInst {}
unsafe impl Send for BatInst {}

#[async_trait]
impl ContentType for Bat
{
    type Inst   = BatInst;
    type Config = BatConfig;

    async fn new_content(&self, conf: BatConfig) -> Result<BatInst>
    {
        let mngr = battery::Manager::new()?;
        let mut bats = mngr.batteries()?;

        match bats.next()
        {
            Some(Ok(bat)) => Ok(BatInst { info: bat, mngr, conf }),
            Some(Err(e))  => Err(note!(e, "Could not get battery")),
            None          => Err(err!("Could not find any batteries"))
        }
    }
}

use battery::units::{
    power::watt,
    ratio::percent,
    electric_potential::volt
};

#[async_trait]
impl Content for BatInst
{
    async fn status(&mut self) -> io::Result<block::Status>
    {
        self.mngr.refresh(&mut self.info)
            .map_err(|e| io::Error::new(
                io::ErrorKind::Other,
                format!("Could not refresh battery status: {}", e)
            ))?;

        let perc  = self.info.state_of_charge().get::<percent>();
        let rate  = self.info.energy_rate().get::<watt>();
        let volts = self.info.voltage().get::<volt>();

        let tmp;

        let state: &str = match &self.conf.icons
            {
                Some(icons) => icons.get(self.info.state(), perc),
                None        => { tmp = format!("{}", self.info.state()); &tmp }
            };

        Ok(block::Status {
            full: Some(format!(
                "{} {:3.0}% {:4.1}W {:4.1}V",
                state, perc, rate, volts
            )),
            short: Some(format!(
                "{} {:3.0}%",
                state, perc
            )),
            urgent: false
        })
    }
}
