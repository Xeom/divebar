//! # Content
//!
//! `Bar`s are made of `Block`s of `Content`.
//!
//! Implementing the `Content` and `ContentType` traits for a pair of types will
//! implement a new type of block.
//!
//! Using the `.with_content_type()` method on a `Bar` object can then allow
//! this new type of block to be specified in a config and used.
//!

use super::*;

mod time;
pub use time::Time;

mod bat;
pub use bat::Bat;

mod backlight;
pub use backlight::Backlight;

mod wifi;
pub use wifi::Wifi;

//mod weather;
//pub use weather::Weather;

//mod volume;
//pub use volume::Volume;

/// A trait implemented by types of content block.
#[async_trait]
pub trait ContentType: Sync + Send
{
    /// A type containing configuration for this type of block.
    ///
    /// On instantiation, a value of this type is deserialized from the config
    /// file.
    ///
    type Config: for<'de> serde::Deserialize<'de> + Sync + Send;

    /// A type for instances of this block.
    type Inst: Sized + Content + 'static;

    /// Create a new instance of this block type with a given config.
    ///
    /// # Parameters
    ///
    ///  * `config` - The configuration to use for the new instance.
    ///
    async fn new_content(&self, config: Self::Config) -> Result<Self::Inst>;
}

/// A trait implemented by instances of content blocks in the bar.
#[async_trait]
pub trait Content: Sync + Send
{
    /// Whether `.status()` is cancel safe.
    ///
    /// If `.status()` is cancel safe, then it may be cancelled to handle a
    /// click event.
    /// If `.status()` takes a long time to complete, and click events are used,
    /// then it should be cancellable.
    ///
    fn status_cancellable(&self) -> bool { true }

    /// Poll and return the latest status.
    ///
    /// Only block if necessary.
    ///
    /// # Cancel Safety
    ///
    /// This function should be cancel safe if `.status_cancellable()` returns
    /// `true`.
    ///
    async fn status(&mut self) -> io::Result<block::Status>;

    /// Respond to a click event.
    async fn click(&mut self) -> io::Result<()> { Ok(()) }

    /// Wait for an update to the state.
    ///
    /// This function is optional, but is useful, as it will not be called
    /// before the first invocation of `.status()`, and so adding delays here
    /// will reduce the lag before an initial status appears.
    ///
    /// # Cancel Safety
    ///
    /// This function must be cancel safe.
    ///
    async fn wait(&mut self) -> io::Result<()> { Ok(()) }
}

/// A `Box<>`-able trait for types of content.
///
/// This is derived from `ContentType`, and should not be directly implemented.
///
#[async_trait]
pub trait ContentTypeBoxed
{
    /// Return a new boxed instance of this type of content.
    async fn new_boxed_content(&self, config: &block::Config)
        -> Result<Box<dyn Content>>;
}

#[async_trait]
impl<T> ContentTypeBoxed for T where T: ContentType
{
    async fn new_boxed_content(&self, config: &block::Config)
        -> Result<Box<dyn Content>>
    {
        use serde::Deserialize;
        // Parse the configuration for the `Content`
        let parsed = <Self as ContentType>::Config::deserialize(
                config.inner.clone()
            )
            .map_err(with_note!("Could not parse config!"))?;

        Ok(Box::new(Self::new_content(self, parsed).await?))
    }
}

#[derive(serde::Deserialize)]
pub struct EmptyConfig { }

