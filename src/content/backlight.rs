use super::*;

use std::path::{Path, PathBuf};

#[derive(serde::Deserialize)]
pub struct Icons
{
    brightness: Vec<String>
}

impl Icons
{
    fn get_brightness(&self, perc: f64) -> &str
    {
        let approx = (perc / 101.0) * self.brightness.len() as f64;
        let ind = approx.floor() as usize;
        self.brightness.get(ind).map(String::as_str).unwrap_or("?")
    }
}

#[derive(serde::Deserialize)]
pub struct BacklightConf
{
    device: PathBuf,
    icons:  Option<Icons>
}

pub struct BacklightInst
{
    conf:           BacklightConf,
    max_brightness: u64,
    inotify_stream: inotify::EventStream<[u8; 256]>
}

pub struct Backlight;

fn max_brightness_path(conf: &BacklightConf) -> PathBuf
{
    conf.device.join("max_brightness")
}

fn current_brightness_path(conf: &BacklightConf) -> PathBuf
{
    conf.device.join("brightness")
}

async fn read_u64(path: impl AsRef<Path>) -> io::Result<u64>
{
    fs::read_to_string(path).await?
        .trim()
        .parse()
        .map_err(|e| io::Error::new(
            io::ErrorKind::Other,
            format!("Error converting to u64: {}", e)
        ))
}

impl BacklightInst
{
    async fn get_percent_brightness(&mut self) -> io::Result<f64>
    {
        let curr = read_u64(current_brightness_path(&self.conf)).await?;
        let max  = self.max_brightness;

        Ok(100.0 * (curr as f64) / (max as f64))
    }
}

#[async_trait]
impl ContentType for Backlight
{
    type Inst   = BacklightInst;
    type Config = BacklightConf;

    async fn new_content(&self, conf: BacklightConf) -> Result<BacklightInst>
    {
        let mut inotify = inotify::Inotify::init()?;
        inotify.add_watch(
            &current_brightness_path(&conf),
            inotify::WatchMask::CLOSE_WRITE
        )?;

        let max_brightness = read_u64(max_brightness_path(&conf)).await?;
        let inotify_stream = inotify.event_stream([0; 256])?;

        Ok(BacklightInst { conf, inotify_stream, max_brightness })
    }
}

#[async_trait]
impl Content for BacklightInst
{
    async fn wait(&mut self) -> io::Result<()>
    {
        if let Some(polled) = self.inotify_stream.next().await
        {
            polled?;
        }
        else
        {
            sleep(Duration::from_secs(1)).await
        }

        Ok(())
    }

    async fn status(&mut self) -> io::Result<block::Status>
    {
        let perc = self.get_percent_brightness().await?;
        let icon = match &self.conf.icons
        {
            Some(icons) => icons.get_brightness(perc),
            None        => "Brightness"
        };

        Ok(block::Status {
            full:   Some(format!("{} {:3.0}%", icon, perc)),
            short:  None,
            urgent: false
        })
    }
}
